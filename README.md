# Créer simplement des pages web avec le modèle Pandoc

## Que fait ce modèle ?

[Ce modèle](https://forge.apps.education.fr/modeles-projets/modele-pandoc) propose un outil simple pour transformer des fichiers textes écrits avec la syntaxe Markdown en pages HTML, grâce à [Pandoc](https://pandoc.org/).

Vous avez simplement à comprendre comment fonctionne la syntaxe Markdown, l'outil s'occupe automatiquement de la conversion de votre fichier en une page web.

Un exemple : le fichier [index.md](https://forge.apps.education.fr/modeles-projets/modele-pandoc/-/blob/main/index.md?plain=1) de ce répertoire crée la page [index.html](https://modeles-projets.forge.apps.education.fr/modele-pandoc/index.html).

## Pour quel usage ?

Ce modèle reste basique et permet de s'initier à la création de pages web en Markdown.
Il est aussi très pratique pour créer rapidement une page web.

:warning: Tous les fichiers Markdown dans votre répertoire sont convertis en HTML, mais les pages ne sont pas liées automatiquement entre elles. Vous pouvez cependant inclure la liste des pages générées sur le site (par exemple sur la page d'accueil définie par le fichier `index.md`).

Si vous souhaitez faire un site complet, avec une structure sans avoir à écrire les liens entre les pages par vous-même, il vaut mieux [utiliser un autre modèle](https://forge.apps.education.fr/modeles-projets).

## Comment utiliser ce modèle ?

Il suffit de réutiliser [ce modèle](https://forge.apps.education.fr/modeles-projets/modele-pandoc) en cliquant sur “Fork” ou “Créer une divergence” en français, ou en clonant le répertoire.

Vous pouvez ensuite créer des fichiers markdown (avec une extension `.md`) dans votre répertoire. Commencez par exemple par changer le fichier `index.md` qui définit votre page d'accueil.

Chaque fichier markdown sera alors automatiquement converti en une page HTML.

## Où se trouve mon site web ?

Si votre nom d'utilisateur est `mon_nom` et que vous avez récupéré ce modèle en nommant votre répertoire `mon_répertoire`, votre site web se trouve à l'adresse :

`https://mon_nom.forge.apps.education.fr/mon_répertoire/`

## Où sont mes pages ?

- Si vous avez créé un fichier `page1.md` à la racine de ce répertoire, votre page web se trouvera à l'adresse : `https://mon_nom.forge.apps.education.fr/mon_répertoire/page1`
- Si vous avez créé un fichier `mondossier/page2.md`, votre page web se trouvera à l'adresse : `https://mon_nom.forge.apps.education.fr/mon_répertoire/mon_dossier/page2`